package com.nexsoft.code20.Models.Repos;


import java.util.ArrayList;

import com.nexsoft.code20.Models.Entities.Email;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmailRepository extends CrudRepository<Email, Integer> {

    public ArrayList<Email> findAll();
}
