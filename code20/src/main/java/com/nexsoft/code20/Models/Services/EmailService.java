package com.nexsoft.code20.Models.Services;

import java.util.ArrayList;

import javax.transaction.TransactionScoped;

import com.nexsoft.code20.Models.Entities.Email;
import com.nexsoft.code20.Models.Repos.EmailRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@TransactionScoped
public class EmailService {

    @Autowired
    EmailRepository emailRepository;

    public void saveAll(ArrayList<Email> listEmail){
    emailRepository.saveAll(listEmail);
    }

    public ArrayList<Email> findAllEmails(){
        return emailRepository.findAll();
    }
    
}
