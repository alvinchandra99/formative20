package com.nexsoft.code20.Controllers;

import java.util.ArrayList;

import com.nexsoft.code20.Models.Entities.Email;
import com.nexsoft.code20.Models.Services.EmailService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    EmailService emailService;

    @PostMapping("/save")
    public String insert(@RequestBody ArrayList<Email> listEmail) {
        try {
            emailService.saveAll(listEmail);
            return "Berhasil Memasukan Data";

        } catch (Exception e) {
            return "Gagal Memasukan Data";
        }
    }

    @GetMapping("/home")
    public ModelAndView home() {
        ModelAndView mv = new ModelAndView("index");
        mv.addObject("listEmail", emailService.findAllEmails());
        return mv;
    }

}